<!DOCTYPE html>
<html>
<head>
    <?php include 'base/head.php'; ?>
    <title>Download Lagu MP3 Indo dan Barat Terlengkap 2020 - Metrolagu VIP</title>
</head>
<body>
    <?php include 'base/loader.php'; ?>
    <?php include 'base/navbar.php'; ?>
    <?php include 'base/navdrawer.php'; ?>
    
    <div class="position-relative d-flex justify-content-center">
        <!-- Main content -->
        <div class="container" style="margin-top: 56px;">
            
            <!-- Player -->
            <div class="row">
                <div class="col-12 bg-white shadow-sm">
                    <ul class="nav nav-pills mb-3 br10 white p-1 mt-3" id="pills-tab" role="tablist" style="background: #F1F1F6;">
                        <li class="nav-item" style="width: 50%;">
                          <a class="nav-link active text-center" id="pills-audio-tab" data-toggle="pill" href="#pills-audio" role="tab" aria-controls="pills-audio" aria-selected="true">Audio</a>
                        </li>
                        <li class="nav-item" style="width: 50%;">
                          <a class="nav-link text-center" id="pills-video-tab" data-toggle="pill" href="#pills-video" role="tab" aria-controls="pills-video" aria-selected="false">Video</a>
                        </li>
                      </ul>
                      <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-audio" role="tabpanel" aria-labelledby="pills-audio-tab">
                            <div class="card bg-dark text-white br10 overflow-hidden mb-2">                                    
                                <img src="https://i.ytimg.com/vi/qnwBxCxpw_Q/mqdefault.jpg" class="card-img overlay-img" alt="image">
                                <div class="card-img-overlay">
                                    <div class="w-100 h-100 position-absolute" style="left:0; top:0; opacity: 0.55; background: black;"></div>
                                    <h5 class="position-relative card-title">BENEE</h5>
                                    <p class="position-relative card-text">Supalonely (Lyrics) ft. Gus Dapperton</p>
                                    <p class="position-relative card-text"><small>00:03:40</small></p>
                                </div>
                            </div>    
                            <iframe src="https://en.dog/audio.php?v=JKIQuYmMaKs" width="100%" height="60px" frameborder="0" class="my-3" /></iframe>
                        </div>
                        <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab">
                            <div class="row mb-3">
                                <div class="col-12">
                                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/qnwBxCxpw_Q?autoplay=0&showinfo=0&iv_load_policy=3&rel=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                      </div>
                </div>
            </div>

            <div class="container mt-3">
                <div class="row">
                    <!-- Like dislike -->
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 pl-0 pr-2">
                                <div class="p-3 bg-white shadow-sm br10">
                                    <p class="f13 mb-0 tgray">
                                        Like
                                    </p>
                                    <p class="f24 mb-0 tgreen font-weight-bold">
                                        234.22rb
                                    </p>
                                </div>
                            </div>
                            <div class="col-6 pl-2 pr-0">
                                <div class="p-3 bg-white shadow-sm br10">
                                    <p class="f13 mb-0 tgray">
                                        Dislike
                                    </p>
                                    <p class="f24 mb-0 tred font-weight-bold">
                                        22rb
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Link download -->
                    <div class="col-12 bg-white shadow-sm br10 overflow-hidden mt-3">
                        <div class="row">
                            <div class="col-12 f15 tdark text-center p-3 border-bottom">
                                Link Download
                            </div>
                            <div class="col-12 p-3 border-bottom text-center">
                                <p class="f15 tgray">
                                    Dua Lipa - Boys Wil Be Boys
                                </p>
                                <a href="#" class="btn btn-primary br10 f13">DOWNLOAD</a>
                            </div>
                            <div class="col-12 p-3 f13 tgray text-center">
                                122.5 x Download
                            </div>
                        </div>
                    </div>
                    <!-- Lirik lagu desc -->
                    <div class="col-12 bg-white shadow-sm br10 mt-3">
                        <div class="row liriklagu">
                            <div class="col-12 border-bottom">
                                <button class="btn-desc w-100 text-left py-3 px-0 f15" type="button" data-toggle="collapse" data-target="#liriklagu" aria-expanded="false" aria-controls="liriklagu">
                                    Lirik Lagu
                                </button>
                                <div class="collapse" id="liriklagu">
                                <div class="pb-3 f15 tgray">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn-desc w-100 text-left py-3 px-0 f15" type="button" data-toggle="collapse" data-target="#desc" aria-expanded="false" aria-controls="desc">
                                    Desc
                                </button>
                                <div class="collapse" id="desc">
                                <div class="pb-3 f15 tgray">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Lagu lainnya -->
            <div class="container mt-3">
                <div class="row lagulainnya">
                    <div class="col-12 bg-white p-0 br10 mt-2">
                        <div class="card bg-dark text-white br10 overflow-hidden mb-2">                                    
                            <img src="https://i.ytimg.com/vi/qnwBxCxpw_Q/mqdefault.jpg" class="card-img overlay-img" alt="image">
                            <div class="card-img-overlay">
                                <div class="w-100 h-100 position-absolute" style="left:0; top:0; opacity: 0.55; background: black;"></div>
                                <h5 class="position-relative card-title">BENEE</h5>
                                <p class="position-relative card-text tgray"><small>29 January 2020</small></p>
                            </div>
                        </div> 
                    </div>
                    <div class="col-12 bg-white p-0 br10 mt-2">
                        <div class="card bg-dark text-white br10 overflow-hidden mb-2">                                    
                            <img src="https://i.ytimg.com/vi/qnwBxCxpw_Q/mqdefault.jpg" class="card-img overlay-img" alt="image">
                            <div class="card-img-overlay">
                                <div class="w-100 h-100 position-absolute" style="left:0; top:0; opacity: 0.55; background: black;"></div>
                                <h5 class="position-relative card-title">BENEE</h5>
                                <p class="position-relative card-text tgray"><small>29 January 2020</small></p>
                            </div>
                        </div> 
                    </div>
                    <div class="col-12 bg-white p-0 br10 mt-2">
                        <div class="card bg-dark text-white br10 overflow-hidden mb-2">                                    
                            <img src="https://i.ytimg.com/vi/qnwBxCxpw_Q/mqdefault.jpg" class="card-img overlay-img" alt="image">
                            <div class="card-img-overlay">
                                <div class="w-100 h-100 position-absolute" style="left:0; top:0; opacity: 0.55; background: black;"></div>
                                <h5 class="position-relative card-title">BENEE</h5>
                                <p class="position-relative card-text tgray"><small>29 January 2020</small></p>
                            </div>
                        </div> 
                    </div>
                    <div class="col-12 bg-white p-0 br10 mt-2">
                        <div class="card bg-dark text-white br10 overflow-hidden mb-2">                                    
                            <img src="https://i.ytimg.com/vi/qnwBxCxpw_Q/mqdefault.jpg" class="card-img overlay-img" alt="image">
                            <div class="card-img-overlay">
                                <div class="w-100 h-100 position-absolute" style="left:0; top:0; opacity: 0.55; background: black;"></div>
                                <h5 class="position-relative card-title">BENEE</h5>
                                <p class="position-relative card-text tgray"><small>29 January 2020</small></p>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'base/footer.php'; ?>
</body>
<script src="assets/js/jquery.js"></script>
<script>
    $(document).ready(function(){
        // $('.liriklagu div:first-child').toggleClass('border-bottom');
        $('.liriklagu div:first-child').click(function(){
            let n = 0;
            if(n == 0){
                $(this).removeClass('border-bottom');
                n += 1;
            }else if(n == 1){
                $(this).addClass('border-bottom');
                n = 0;
            }
        });
    });
</script>
</html>

