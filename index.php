<!DOCTYPE html>
<html>
<head>
    <?php include 'base/head.php'; ?>
    <title>Download Lagu MP3 Indo dan Barat Terlengkap 2020 - Metrolagu VIP</title>
</head>
<body>
    <?php include 'base/loader.php'; ?>
    <?php include 'base/navbar.php'; ?>
    <?php include 'base/navdrawer.php'; ?>
    
    <div class="position-relative d-flex justify-content-center">
        <!-- Background blue -->
        <div class="bg-primary position-absolute container w-100" style="height: 140px; z-index: -1; top: 56px;">
        </div>
        <!-- Main content -->
        <div class="container pt-2" style="margin-top: 56px;">
            
            <!-- Selamat datang di metrolagu -->
            <div class="">
                <div class="container bg-white br10 px-4 pb-3 shadow-sm" style="padding-top: 43px; height: 246px;">
                    <div class="">
                        <p class="mb-0 tgray" style="font-size: 15px;">Selamat datang di</p>
                        <p class="my-0 f32 font-weight-bold tdark" style="line-height: 34px;">Metrolagu</p>
                    </div>
                    <hr style="border: 0.5px solid #dcdce9; margin-top: 34px;" class="w-100">
                    <div class="row mt-3">
                        <div class="col-3">
                            <a href="#" class="text-decoration-none d-flex flex-column align-items-center justify-content-center">
                                <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 48px; height: 48px; background: #ff396f">
                                    <svg height="24px" viewBox="-32 0 512 512" width="24px" xmlns="http://www.w3.org/2000/svg" fill="#fff"><path d="m432.734375 112.464844c-53.742187 0-97.464844-43.722656-97.464844-97.464844 0-8.285156-6.71875-15-15-15h-80.335937c-8.285156 0-15 6.714844-15 15v329.367188c0 31.59375-25.703125 57.296874-57.300782 57.296874-31.59375 0-57.296874-25.703124-57.296874-57.296874 0-31.597657 25.703124-57.300782 57.296874-57.300782 8.285157 0 15-6.714844 15-15v-80.335937c0-8.28125-6.714843-15-15-15-92.433593 0-167.632812 75.203125-167.632812 167.636719 0 92.433593 75.199219 167.632812 167.632812 167.632812 92.433594 0 167.636719-75.199219 167.636719-167.632812v-145.792969c29.855469 15.917969 63.074219 24.226562 97.464844 24.226562 8.285156 0 15-6.714843 15-15v-80.335937c0-8.28125-6.714844-15-15-15zm-15 79.714844c-32.023437-2.664063-62.433594-13.851563-88.707031-32.75-4.566406-3.289063-10.589844-3.742188-15.601563-1.171876-5.007812 2.5625-8.15625 7.71875-8.15625 13.347657v172.761719c0 75.890624-61.746093 137.632812-137.636719 137.632812-75.890624 0-137.632812-61.742188-137.632812-137.632812 0-70.824219 53.773438-129.328126 122.632812-136.824219v50.8125c-41.015624 7.132812-72.296874 42.984375-72.296874 86.011719 0 48.136718 39.160156 87.300781 87.296874 87.300781 48.140626 0 87.300782-39.164063 87.300782-87.300781v-314.367188h51.210937c6.871094 58.320312 53.269531 104.71875 111.589844 111.589844zm0 0"></path></svg>
                                </div>
                                <p class="f12 text-dark mt-2">TikTok</p>
                            </a>
                        </div>
                        <div class="col-3">
                            <a href="#" class="text-decoration-none d-flex flex-column align-items-center justify-content-center">
                                <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 48px; height: 48px; background: #6236ff">
                                    <img src="assets/icon/cloud.svg" class="cloud" width="24px">
                                </div>
                                <p class="f12 text-dark mt-2">Mood</p>
                            </a>
                        </div>
                        <div class="col-3">
                            <a href="#" class="text-decoration-none d-flex flex-column align-items-center justify-content-center">
                                <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 48px; height: 48px; background: #1dcc70">
                                    <img src="assets/icon/game.svg" class="cloud" width="24px">
                                </div>
                                <p class="f12 text-dark mt-2">Santuy</p>
                            </a>
                        </div>
                        <div class="col-3">
                            <a href="#" class="text-decoration-none d-flex flex-column align-items-center justify-content-center">
                                <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 48px; height: 48px; background: #ffb400">
                                    <img src="assets/icon/sad.svg" class="cloud" width="24px">
                                </div>
                                <p class="f12 text-dark mt-2">Galau</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top 50 -->
            <div class="container mt-3">
                <div class="row">
                    <div class="col-6 pl-0 pr-2">
                        <a href="#" class="text-decoration-none" title="Lagu terpopuler barat bulan ini">
                            <div class="shadow-sm py-3 px-4 bg-white br10">
                                <p class="f13 tgray mb-0">
                                    Top 50
                                </p>
                                <p class="f13 tgreen font-weight-bold f24 mb-0">
                                    Global
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-6 pr-0 pl-2">
                        <a href="#" class="text-decoration-none" title="Lagu terpopuler indonesia bulan ini">
                            <div class="shadow-sm py-3 px-4 bg-white br10">
                                <p class="f13 tgray mb-0">
                                    Top 50
                                </p>
                                <p class="f13 tred font-weight-bold f24 mb-0">
                                    Indonesia
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Download lagu terbaru -->
            <div class="container mcontainer">
                <p class="f17 tdark text-center font-weight-bold">
                    Download Lagu Terbaru
                </p>
                <div class="row bg-white br10 shadow-sm overflow-hidden py-1 listlagu">
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Lagi hits -->
            <div class="container mcontainer">
                <div class="row">
                    <div class="col-12 p-0">
                        <h2 class="tdark f20 font-weight-bold float-left">
                            Lagi Hits
                        </h2>
                        <a href="#" class="text-decoration-none float-right">
                            Lihat Semua
                        </a>
                    </div>
                </div>
                <div class="row laguhitcontainer">
                    <div class="col-12 p-0 mt-2">
                        <div class="bg-white br10 shadow-sm p-3">
                            <a href="#" class="text-decoration-none">
                                <div class="d-flex flex-row align-items-center justify-content-between">
                                    <div class="d-flex flex-row w-100">
                                        <div class="imgmusic br10 overflow-hidden">
                                            <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                        </div>
                                        <div class="ml-3 d-flex flex-column justify-content-center">
                                            <p class="f15 tdark mb-0">
                                                Sunday Beast
                                            </p>
                                            <p class="f11 tgray mb-0">
                                                WINNER
                                            </p>
                                        </div>
                                    </div>
                                    <div class="mx-3">
                                        <p class="f15 mb-0 text-primary font-weight-bold">
                                            1
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 p-0 mt-2">
                        <div class="bg-white br10 shadow-sm p-3">
                            <a href="#" class="text-decoration-none">
                                <div class="d-flex flex-row align-items-center justify-content-between">
                                    <div class="d-flex flex-row w-100">
                                        <div class="imgmusic br10 overflow-hidden">
                                            <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                        </div>
                                        <div class="ml-3 d-flex flex-column justify-content-center">
                                            <p class="f15 tdark mb-0">
                                                Sunday Beast
                                            </p>
                                            <p class="f11 tgray mb-0">
                                                WINNER
                                            </p>
                                        </div>
                                    </div>
                                    <div class="mx-3">
                                        <p class="f15 mb-0 text-primary font-weight-bold">
                                            2
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 p-0 mt-2">
                        <div class="bg-white br10 shadow-sm p-3">
                            <a href="#" class="text-decoration-none">
                                <div class="d-flex flex-row align-items-center justify-content-between">
                                    <div class="d-flex flex-row w-100">
                                        <div class="imgmusic br10 overflow-hidden">
                                            <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                        </div>
                                        <div class="ml-3 d-flex flex-column justify-content-center">
                                            <p class="f15 tdark mb-0">
                                                Sunday Beast
                                            </p>
                                            <p class="f11 tgray mb-0">
                                                WINNER
                                            </p>
                                        </div>
                                    </div>
                                    <div class="mx-3">
                                        <p class="f15 mb-0 text-primary font-weight-bold">
                                            3
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 p-0 mt-2">
                        <div class="bg-white br10 shadow-sm p-3">
                            <a href="#" class="text-decoration-none">
                                <div class="d-flex flex-row align-items-center justify-content-between">
                                    <div class="d-flex flex-row w-100">
                                        <div class="imgmusic br10 overflow-hidden">
                                            <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                        </div>
                                        <div class="ml-3 d-flex flex-column justify-content-center">
                                            <p class="f15 tdark mb-0">
                                                Sunday Beast
                                            </p>
                                            <p class="f11 tgray mb-0">
                                                WINNER
                                            </p>
                                        </div>
                                    </div>
                                    <div class="mx-3">
                                        <p class="f15 mb-0 text-primary font-weight-bold">
                                            4
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div> 
            <!-- Recent search -->
            <div class="container mcontainer">
                <p class="f17 tdark text-center font-weight-bold">
                    Recent Search
                </p>
                <div class="row bg-white br10 shadow-sm overflow-hidden py-1 listlagu">
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'base/footer.php'; ?>
</body>
<script src="assets/js/jquery.js"></script>
</html>

