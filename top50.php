<!DOCTYPE html>
<html>
<head>
    <?php include 'base/head.php'; ?>
    <title>Download Lagu MP3 Indo dan Barat Terlengkap 2020 - Metrolagu VIP</title>
</head>
<body>
    <?php include 'base/loader.php'; ?>
    <?php include 'base/navbar.php'; ?>
    <?php include 'base/navdrawer.php'; ?>
    
    <div class="position-relative d-flex justify-content-center">
        <!-- Main content -->
        <div class="container" style="margin-top: 56px;">
            <div class="container">
                <div class="row">
                    <!-- Top 50 -->
                    <div class="container mt-3">
                        <div class="row">
                            <div class="col-6 pl-0 pr-2">
                                <a href="#" class="text-decoration-none" title="Lagu terpopuler barat bulan ini">
                                    <div class="shadow-sm py-3 px-4 bg-white br10">
                                        <p class="f13 tgray mb-0">
                                            Top 50
                                        </p>
                                        <p class="f13 tgreen font-weight-bold f24 mb-0">
                                            Global
                                        </p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 pr-0 pl-2">
                                <a href="#" class="text-decoration-none" title="Lagu terpopuler indonesia bulan ini">
                                    <div class="shadow-sm py-3 px-4 bg-white br10">
                                        <p class="f13 tgray mb-0">
                                            Top 50
                                        </p>
                                        <p class="f13 tred font-weight-bold f24 mb-0">
                                            Indonesia
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- Content -->
                    <div class="container mcontainer">
                        <div class="row">
                            <div class="col-12 p-0">
                                <h2 class="tdark f20 font-weight-bold float-left">
                                    Top 50 Lagu Barat
                                </h2>
                                
                            </div>
                        </div>
                        <div class="row laguhitcontainer">
                            <div class="col-12 p-0 mt-2">
                                <div class="bg-white br10 shadow-sm p-3">
                                    <a href="#" class="text-decoration-none">
                                        <div class="d-flex flex-row align-items-center justify-content-between">
                                            <div class="d-flex flex-row w-100">
                                                <div class="imgmusic br10 overflow-hidden">
                                                    <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                                </div>
                                                <div class="ml-3 d-flex flex-column justify-content-center">
                                                    <p class="f15 tdark mb-0">
                                                        Sunday Beast
                                                    </p>
                                                    <p class="f11 tgray mb-0">
                                                        WINNER
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="mx-3">
                                                <p class="f15 mb-0 text-primary font-weight-bold">
                                                    1
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 p-0 mt-2">
                                <div class="bg-white br10 shadow-sm p-3">
                                    <a href="#" class="text-decoration-none">
                                        <div class="d-flex flex-row align-items-center justify-content-between">
                                            <div class="d-flex flex-row w-100">
                                                <div class="imgmusic br10 overflow-hidden">
                                                    <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                                </div>
                                                <div class="ml-3 d-flex flex-column justify-content-center">
                                                    <p class="f15 tdark mb-0">
                                                        Sunday Beast
                                                    </p>
                                                    <p class="f11 tgray mb-0">
                                                        WINNER
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="mx-3">
                                                <p class="f15 mb-0 text-primary font-weight-bold">
                                                    2
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 p-0 mt-2">
                                <div class="bg-white br10 shadow-sm p-3">
                                    <a href="#" class="text-decoration-none">
                                        <div class="d-flex flex-row align-items-center justify-content-between">
                                            <div class="d-flex flex-row w-100">
                                                <div class="imgmusic br10 overflow-hidden">
                                                    <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                                </div>
                                                <div class="ml-3 d-flex flex-column justify-content-center">
                                                    <p class="f15 tdark mb-0">
                                                        Sunday Beast
                                                    </p>
                                                    <p class="f11 tgray mb-0">
                                                        WINNER
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="mx-3">
                                                <p class="f15 mb-0 text-primary font-weight-bold">
                                                    3
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 p-0 mt-2">
                                <div class="bg-white br10 shadow-sm p-3">
                                    <a href="#" class="text-decoration-none">
                                        <div class="d-flex flex-row align-items-center justify-content-between">
                                            <div class="d-flex flex-row w-100">
                                                <div class="imgmusic br10 overflow-hidden">
                                                    <img src="https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/bd/c0/2b/bdc02bc3-22a0-c59e-f832-244debc97007/19UMGIM68846.rgb.jpg/48x48bb.png" width="48px">
                                                </div>
                                                <div class="ml-3 d-flex flex-column justify-content-center">
                                                    <p class="f15 tdark mb-0">
                                                        Sunday Beast
                                                    </p>
                                                    <p class="f11 tgray mb-0">
                                                        WINNER
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="mx-3">
                                                <p class="f15 mb-0 text-primary font-weight-bold">
                                                    4
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <?php include 'base/footer.php'; ?>
</body>
<script src="assets/js/jquery.js"></script>
<script>
    $(document).ready(function(){
        // $('.liriklagu div:first-child').toggleClass('border-bottom');
        $('.liriklagu div:first-child').click(function(){
            let n = 0;
            if(n == 0){
                $(this).removeClass('border-bottom');
                n += 1;
            }else if(n == 1){
                $(this).addClass('border-bottom');
                n = 0;
            }
        });
    });
</script>
</html>

