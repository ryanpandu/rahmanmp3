<footer class="footercontainer mcontainer p-0">
    <div class="border-top bg-white p-3">
        <div class="text-center">
            <p class="mb-1 f12 tdark font-weight-bold">
                Copyright © Metrolagu 2020. All Right Reserved.
            </p>
            <p class="mb-0 f12 tgray">
                Metrolagu.vip
            </p>
        </div>
    </div>
</footer>