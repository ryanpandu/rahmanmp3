<div id="navdrawercontainer" class="vw-100 vh-100 position-fixed" style="z-index: 99; display: none;">
    <div class="bg-white h-100 position-absolute navdrawermenu pb-4" style="width: 300px; z-index: 99; left: -200px; overflow-y:scroll;">
        <div class="container pt-4 pb-2" style="background: #222;">
            <p class="f23 text-white mb-2 font-weight-bold">
                metrolagu.vip
            </p>
            <p class="f13" style="line-height: 25px; color:#A1A1A1;">
                I love cheese, especially cow paneer. Cheeseburger monterey jack cow the big cheese pecorino taleggio everyone loves cheese triangles. Everyone loves monterey jack pepper jack cheese.
            </p>
        </div>
        <div class="container mt-3">
            <div class="row">
                <div class="col-12 tgray f13 font-weight-bold">
                    Playlist
                </div>
                <div class="col-12 mt-4">
                    <a href="#" class="text-decoration-none d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 36px; height: 36px; background: #007bff; border-radius: 50%;">
                                <svg height="24px" viewBox="-32 0 512 512" width="16px" xmlns="http://www.w3.org/2000/svg" fill="#fff"><path d="m432.734375 112.464844c-53.742187 0-97.464844-43.722656-97.464844-97.464844 0-8.285156-6.71875-15-15-15h-80.335937c-8.285156 0-15 6.714844-15 15v329.367188c0 31.59375-25.703125 57.296874-57.300782 57.296874-31.59375 0-57.296874-25.703124-57.296874-57.296874 0-31.597657 25.703124-57.300782 57.296874-57.300782 8.285157 0 15-6.714844 15-15v-80.335937c0-8.28125-6.714843-15-15-15-92.433593 0-167.632812 75.203125-167.632812 167.636719 0 92.433593 75.199219 167.632812 167.632812 167.632812 92.433594 0 167.636719-75.199219 167.636719-167.632812v-145.792969c29.855469 15.917969 63.074219 24.226562 97.464844 24.226562 8.285156 0 15-6.714843 15-15v-80.335937c0-8.28125-6.714844-15-15-15zm-15 79.714844c-32.023437-2.664063-62.433594-13.851563-88.707031-32.75-4.566406-3.289063-10.589844-3.742188-15.601563-1.171876-5.007812 2.5625-8.15625 7.71875-8.15625 13.347657v172.761719c0 75.890624-61.746093 137.632812-137.636719 137.632812-75.890624 0-137.632812-61.742188-137.632812-137.632812 0-70.824219 53.773438-129.328126 122.632812-136.824219v50.8125c-41.015624 7.132812-72.296874 42.984375-72.296874 86.011719 0 48.136718 39.160156 87.300781 87.296874 87.300781 48.140626 0 87.300782-39.164063 87.300782-87.300781v-314.367188h51.210937c6.871094 58.320312 53.269531 104.71875 111.589844 111.589844zm0 0"></path></svg>
                            </div>
                            <p class="f15 tdark ml-3 mb-0">TikTok Playlist</p>
                        </div>
                        <div class="rounded-circle f12 text-white d-flex justify-content-center align-items-center mr-4" style="width: 22px; height: 22px; background: #6236ff;">
                            10
                        </div>
                    </a>
                </div>
                <div class="col-12 mt-4">
                    <a href="#" class="text-decoration-none d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 36px; height: 36px; background: #28a745; border-radius: 50%;">
                                <img src="assets/icon/cloud.svg" class="cloud" width="16px">
                            </div>
                            <p class="f15 tdark ml-3 mb-0">Mood</p>
                        </div>
                        <!-- <div class="rounded-circle f12 text-white d-flex justify-content-center align-items-center mr-4" style="width: 22px; height: 22px; background: #6236ff;">
                            10
                        </div> -->
                    </a>
                </div>
                <div class="col-12 mt-4">
                    <a href="#" class="text-decoration-none d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 36px; height: 36px; background: #754aed; border-radius: 50%;">
                                <img src="assets/icon/game.svg" class="cloud" width="16px">
                            </div>
                            <p class="f15 tdark ml-3 mb-0">Santuy</p>
                        </div>
                        <!-- <div class="rounded-circle f12 text-white d-flex justify-content-center align-items-center mr-4" style="width: 22px; height: 22px; background: #6236ff;">
                            10
                        </div> -->
                    </a>
                </div>
                <div class="col-12 mt-4">
                    <a href="#" class="text-decoration-none d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 36px; height: 36px; background: #222; border-radius: 50%;">
                                <img src="assets/icon/sad.svg" class="cloud" width="16px">
                            </div>
                            <p class="f15 tdark ml-3 mb-0">Baper</p>
                        </div>
                        <!-- <div class="rounded-circle f12 text-white d-flex justify-content-center align-items-center mr-4" style="width: 22px; height: 22px; background: #6236ff;">
                            10
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 tgray f13 font-weight-bold">
                    Explore
                </div>
                <div class="col-12 mt-4">
                    <a href="#" class="text-decoration-none d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 36px; height: 36px; background: #007bff; border-radius: 50%;">
                                <img src="assets/icon/fire.svg">
                            </div>
                            <p class="f15 tdark ml-3 mb-0">Top 50 Indonesia</p>
                        </div>
                        <!-- <div class="rounded-circle f12 text-white d-flex justify-content-center align-items-center mr-4" style="width: 22px; height: 22px; background: #6236ff;">
                            10
                        </div> -->
                    </a>
                </div>
                <div class="col-12 mt-4">
                    <a href="#" class="text-decoration-none d-flex flex-row justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <div class="icon-wrapper br10 d-flex align-items-center justify-content-center" style="width: 36px; height: 36px; background: #007bff; border-radius: 50%;">
                                <img src="assets/icon/fire.svg">
                            </div>
                            <p class="f15 tdark ml-3 mb-0">Top 50 Barat</p>
                        </div>
                        <!-- <div class="rounded-circle f12 text-white d-flex justify-content-center align-items-center mr-4" style="width: 22px; height: 22px; background: #6236ff;">
                            10
                        </div> -->
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-dark w-100 h-100 position-relative navdrawerblackopacity" style="opacity: 0.55; z-index: 98;">
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#buttonnavdrawer').click(function(){
            $('#navdrawercontainer').fadeIn(250);
            $(".navdrawermenu").animate({left: '0'});
        });
        $('.navdrawerblackopacity').click(function(){
            $(".navdrawermenu").animate({left: '-200'});
            $('#navdrawercontainer').fadeOut(250);
        });
    });
</script>