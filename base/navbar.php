<nav class="bg-primary position-fixed fixed-top" style="z-index:98;">
    <div class="container py-2 position-relative d-flex align-items-center" style="height: 56px;">
        <div class="d-none flex-row align-items-center searchcontainer w-100">
            <form class="w-100">
                <input type="text" placeholder="Ketik judul lagu..." class="form-control px-2" style="border-radius: 10px; border: none; height: 36px;">
            </form>
            <i id="buttonclose" class="material-icons pointer text-white f28 font-weight-bold ml-2">close</i>
        </div>
        <div class="d-flex flex-row justify-content-between align-items-center w-100 nosearchcontainer">
            <i id="buttonnavdrawer" class="material-icons text-white pointer ">menu</i>
            <a href="#" class="text-white text-decoration-none logometrolagu" style="font-size: 17px;">
                <img src="assets/img/logo.png" class="" width="23.16px" height="24">
                metrolagu
            </a>
            <i id="buttonsearch" class="material-icons text-white pointer">search</i>
        </div>
    </div>
</nav>
<script>
$(document).ready(function(){
    //Navbar search
    $('#buttonsearch').click(function(){
        $('.nosearchcontainer').fadeOut(250, function(){
            $(this).removeClass('d-flex');
            $('.searchcontainer').addClass('d-flex');
            $('.searchcontainer').removeClass('d-none');
            $('.searchcontainer').fadeIn(100);
        });
    });
    $('#buttonclose').click(function(){
        $('.searchcontainer').fadeOut(250, function(){
            $(this).removeClass('d-flex');
            $('.nosearchcontainer').addClass('d-flex');
            $('.nosearchcontainer').removeClass('d-none');
            $('.nosearchcontainer').fadeIn(100);
        });
    });
});
</script>