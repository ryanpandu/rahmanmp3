<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="description" content="MetroLagu adalah situs download lagu terlengkap 2020, terupdate, dan dengan kualitas audio terbaik. kami mengumpulkan video dari youtube dan meneruskan ke situs converter mp3." />
<link rel="canonical" href="https://metrolagu.vip/" />
<link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="assets/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="assets/img/ms-icon-144x144.png">
<meta name="theme-color" content="#00aaff">

<!-- Module -->
<!-- Material icons -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- Jquery -->
<script src="assets/js/jquery.js"></script>
<!-- Bootstrap -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<script src="assets/js/popper.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- Css -->
<link rel="stylesheet" href="assets/css/color.css">
<link rel="stylesheet" href="assets/css/text.css">
<link rel="stylesheet" href="assets/css/preferences.css">
<link rel="stylesheet" href="assets/css/style.css">

<!-- Javascript -->
<script src="assets/js/main.js"></script>

