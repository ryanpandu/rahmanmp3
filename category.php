<!DOCTYPE html>
<html>
<head>
    <?php include 'base/head.php'; ?>
    <title>Download Lagu MP3 Indo dan Barat Terlengkap 2020 - Metrolagu VIP</title>
</head>
<body>
    <?php include 'base/loader.php'; ?>
    <?php include 'base/navbar.php'; ?>
    <?php include 'base/navdrawer.php'; ?>
    
    <div class="position-relative d-flex justify-content-center">
        <!-- Main content -->
        <div class="container" style="margin-top: 56px;">
            <div class="container">
                <div class="row">
                    <!-- Top 50 -->
                    <div class="container mt-3">
                        <div class="row">
                            <div class="col-6 pl-0 pr-2">
                                <a href="#" class="text-decoration-none" title="Lagu terpopuler barat bulan ini">
                                    <div class="shadow-sm py-3 px-4 bg-white br10">
                                        <p class="f13 tgray mb-0">
                                            Top 50
                                        </p>
                                        <p class="f13 tgreen font-weight-bold f24 mb-0">
                                            Global
                                        </p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 pr-0 pl-2">
                                <a href="#" class="text-decoration-none" title="Lagu terpopuler indonesia bulan ini">
                                    <div class="shadow-sm py-3 px-4 bg-white br10">
                                        <p class="f13 tgray mb-0">
                                            Top 50
                                        </p>
                                        <p class="f13 tred font-weight-bold f24 mb-0">
                                            Indonesia
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- Banner -->
                    <div class="container mt-3">
                        <div class="row">
                            <div class="col-12 p-0">
                                <div class="card bg-dark text-white br10 overflow-hidden mb-2">                                    
                                    <img src="https://images.indianexpress.com/2019/05/tiktok-759.jpg?w=759&h=422&imflag=true" class="card-img overlay-img" alt="image">
                                    <div class="card-img-overlay">
                                        <div class="w-100 h-100 position-absolute" style="left:0; top:0; opacity: 0.55; background: black;"></div>
                                        <h5 class="position-relative card-title">TikTok Playlist</h5>
                                        <p class="position-relative card-text">TikTok song that stuck in my head</p>
                                        <p class="position-relative card-text"><small>23 Song</small></p>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div>

                    <!-- Content -->
                    <div class="col-12 bg-white shadow-sm br10 overflow-hidden mt-3">
                        <div class="row">
                            <!-- Song -->
                            <div class="col-12 p-3 border-bottom">
                                <a href="#" class="text-decoration-none">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="bg-primary overflow-hidden br10" style="width: 64px; height: 64px;">
                                            <img src="https://i2.wp.com/ytimg.googleusercontent.com/vi/YvVonQ7LUJ0/mqdefault.jpg?resize=150,150" width="64px">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between ml-3">
                                            <div class="">
                                                <p class="mb-0 f15 tdark">
                                                    <strong>Dua Lipa</strong> - Boys Will Be Boys
                                                </p>
                                                <p class="tgray f12">
                                                    01 November 2019
                                                </p>
                                            </div>
                                            <div class="d-flex">
                                                <div class="rounded-pill bggray d-flex align-items-center pr-2">
                                                    <div class="rounded-circle bg-secondary d-flex justify-content-center align-items-center" style="width: 26px; height: 26px;">
                                                        <img src="assets/icon/chip.svg">
                                                    </div>
                                                    <p class="f13 mb-0 mx-2 tdark">
                                                        Dua Lipa
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 p-3 border-bottom">
                                <a href="#" class="text-decoration-none">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="bg-primary overflow-hidden br10" style="width: 64px; height: 64px;">
                                            <img src="https://i2.wp.com/ytimg.googleusercontent.com/vi/YvVonQ7LUJ0/mqdefault.jpg?resize=150,150" width="64px">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between ml-3">
                                            <div class="">
                                                <p class="mb-0 f15 tdark">
                                                    <strong>Dua Lipa</strong> - Boys Will Be Boys
                                                </p>
                                                <p class="tgray f12">
                                                    01 November 2019
                                                </p>
                                            </div>
                                            <div class="d-flex">
                                                <div class="rounded-pill bggray d-flex align-items-center pr-2">
                                                    <div class="rounded-circle bg-secondary d-flex justify-content-center align-items-center" style="width: 26px; height: 26px;">
                                                        <img src="assets/icon/chip.svg">
                                                    </div>
                                                    <p class="f13 mb-0 mx-2 tdark">
                                                        Dua Lipa
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 p-3 border-bottom">
                                <a href="#" class="text-decoration-none">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="bg-primary overflow-hidden br10" style="width: 64px; height: 64px;">
                                            <img src="https://i2.wp.com/ytimg.googleusercontent.com/vi/YvVonQ7LUJ0/mqdefault.jpg?resize=150,150" width="64px">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between ml-3">
                                            <div class="">
                                                <p class="mb-0 f15 tdark">
                                                    <strong>Dua Lipa</strong> - Boys Will Be Boys
                                                </p>
                                                <p class="tgray f12">
                                                    01 November 2019
                                                </p>
                                            </div>
                                            <div class="d-flex">
                                                <div class="rounded-pill bggray d-flex align-items-center pr-2">
                                                    <div class="rounded-circle bg-secondary d-flex justify-content-center align-items-center" style="width: 26px; height: 26px;">
                                                        <img src="assets/icon/chip.svg">
                                                    </div>
                                                    <p class="f13 mb-0 mx-2 tdark">
                                                        Dua Lipa
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 p-3 border-bottom">
                                <a href="#" class="text-decoration-none">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="bg-primary overflow-hidden br10" style="width: 64px; height: 64px;">
                                            <img src="https://i2.wp.com/ytimg.googleusercontent.com/vi/YvVonQ7LUJ0/mqdefault.jpg?resize=150,150" width="64px">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between ml-3">
                                            <div class="">
                                                <p class="mb-0 f15 tdark">
                                                    <strong>Dua Lipa</strong> - Boys Will Be Boys
                                                </p>
                                                <p class="tgray f12">
                                                    01 November 2019
                                                </p>
                                            </div>
                                            <div class="d-flex">
                                                <div class="rounded-pill bggray d-flex align-items-center pr-2">
                                                    <div class="rounded-circle bg-secondary d-flex justify-content-center align-items-center" style="width: 26px; height: 26px;">
                                                        <img src="assets/icon/chip.svg">
                                                    </div>
                                                    <p class="f13 mb-0 mx-2 tdark">
                                                        Dua Lipa
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <!-- Source -->
                            <div class="col-12 p-3 f13 mb-0 tgray">
                                source: youtube.com
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- Recent search -->
            <div class="container mcontainer">
                <p class="f17 tdark text-center font-weight-bold">
                    Recent Search
                </p>
                <div class="row bg-white br10 shadow-sm overflow-hidden py-1 listlagu">
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                    <div class="col-12 border-bottom">
                        <a href="#" class="f15 text-decoration-none">
                            <p class="my-3">Dua Lipa - Boys Will Be Boys</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'base/footer.php'; ?>
</body>
<script src="assets/js/jquery.js"></script>
<script>
    $(document).ready(function(){
        // $('.liriklagu div:first-child').toggleClass('border-bottom');
        $('.liriklagu div:first-child').click(function(){
            let n = 0;
            if(n == 0){
                $(this).removeClass('border-bottom');
                n += 1;
            }else if(n == 1){
                $(this).addClass('border-bottom');
                n = 0;
            }
        });
    });
</script>
</html>

